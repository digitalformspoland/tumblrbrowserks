//
//  DataParserTests.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 13.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "DataParser.h"
#import <OCMock/OCMock.h>

@interface DataParserTests : XCTestCase

@property (nonatomic, strong) id<DataParserProtocol> dataParser;

@end

@implementation DataParserTests

- (void)setUp {
    [super setUp];
    self.dataParser = [[DataParser alloc] init];
}

- (void)tearDown {
    self.dataParser = nil;
    [super tearDown];
}

- (void)testThatParserReturnProperData
{
    NSDictionary *responseObject = @{@"posts" : @[
                                             @{
                                                 @"id": @"86734144176",
                                                 @"url": @"http://digitalforms.tumblr.com/post/86734144176",
                                                 @"url-with-slug": @"http://digitalforms.tumblr.com/post/86734144176/ive-lost-so-much-weight-since-using-garcinia",
                                                 @"type": @"regular",
                                                 @"date-gmt": @"2014-05-24 22:57:22 GMT",
                                                 @"date": @"Sat, 24 May 2014 18:57:22",
                                                 @"bookmarklet": @0,
                                                 @"mobile": @0,
                                                 @"feed-item": @"",
                                                 @"from-feed-id": @0,
                                                 @"unix-timestamp": @1400972242,
                                                 @"format": @"html",
                                                 @"reblog-key": @"0GoiRBVg",
                                                 @"slug": @"ive-lost-so-much-weight-since-using-garcinia",
                                                 @"regular-title": @"I've lost so much weight, since using garcinia!",
                                                 @"regular-body": @"<p><a href=\"http://pic-me.me/qff/\"><img alt=\"image\" src=\"http://i.imgur.com/aDx8Nkb.png\"/></a></p>"
                                                 }
                                             ]};
    
    NSArray *responsePostArray = [responseObject objectForKey:@"posts"];
    
    PostKO *postKO = [self.dataParser parseResponseJSONData:responsePostArray[0]];
    
    XCTAssertNotNil(postKO, @"postKO should be not nil");
    XCTAssertEqual([postKO.tags count], 0, @"The size of the posts array should be 0");
    XCTAssertEqual(postKO.type, RegularTypeKO, @"Type of post should be RegularTypeKO");
    XCTAssertNotNil(postKO.regularTitle, @"Regular title post should be not nil");
    XCTAssertTrue([postKO.regularTitle isEqualToString:@"I've lost so much weight, since using garcinia!"], @"Regular title post should be equal to string");
}

@end
