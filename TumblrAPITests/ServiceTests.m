//
//  ServiceTests.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 13.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OHHTTPStubs.h"
#import "Service.h"

NSString* const kDigitalforms = @"kDigitalforms";

@interface ServiceTests : XCTestCase

@end

@implementation ServiceTests

- (void)setUp {
    [super setUp];
    
    [OHHTTPStubs stubRequestsPassingTest: ^BOOL (NSURLRequest *request) {
        
        NSString *apiRequestURL = [NSString stringWithFormat:@"http://%@.tumblr.com/api/read/json", kDigitalforms];
        NSRange r = [[request.URL absoluteString] rangeOfString:apiRequestURL];
        return r.location != NSNotFound && [[request HTTPMethod] isEqualToString:@"GET"];
    } withStubResponse: ^OHHTTPStubsResponse *(NSURLRequest *request) {
        
        NSString *filename = @"thumblr.json";
        NSString * jsonPath = [[NSBundle bundleForClass:[self class]] pathForResource:[filename stringByDeletingPathExtension] ofType:[filename pathExtension]];
        NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath];
        
        NSData *key_data = [@"var tumblr_api_read = " dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableData *data = [NSMutableData dataWithData:key_data];
        [data appendData:jsonData];
        
        return [OHHTTPStubsResponse responseWithData:jsonData statusCode:200 headers:@{ @"Content-Type":@"text/javascript" }];
    }];
}

- (void)tearDown {
    [OHHTTPStubs removeAllStubs];
    [super tearDown];
}

- (void)testThatServiceShouldReturnJson {
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing"];
    
    Service *service = [[Service alloc] init];
    [service downloadTumblrDataForSearchQueryString:kDigitalforms withCompletion:^(id responseObject) {
        [expectation fulfill];
        XCTAssertNotNil(responseObject, @"Response should not be nil");
        XCTAssertTrue([responseObject isKindOfClass:[NSDictionary class]], @"Response should be Dictionary kind of class");
    } failure:^(NSError *error) {
        [expectation fulfill];
        XCTFail(@"Error %@", error);
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}



@end
