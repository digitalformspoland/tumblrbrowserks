//
//  InteractorProtocol.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 04.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol InteractorProtocol <NSObject>

typedef void(^completion)(id responseObject);
typedef void(^failure)(NSError* error);

-(void)getDataFormServiceForQueryString:(NSString*)string withCompletionBlock:(completion)completionBlock failureBlock:(failure)failureBlock;


@end