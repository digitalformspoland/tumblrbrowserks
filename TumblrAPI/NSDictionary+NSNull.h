//
//  NSDictionary+NSNull.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 13.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSNull)

- (id)valueOrNilForKey:(NSString *)key;

@end
