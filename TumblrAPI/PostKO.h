//
//  PostKO.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 04.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostKO : NSObject

typedef enum {RegularTypeKO = 1, PhotoTypeKO = 2} TypeEnum;

@property (copy, nonatomic) NSString *bookmarklet;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *dateGMT;
@property (copy, nonatomic) NSString *identifier;
@property (copy, nonatomic) NSString *reblogKey;
@property (copy, nonatomic) NSString *regularBody;
@property (copy, nonatomic) NSString *regularTitle;
@property (copy, nonatomic) NSString *slug;
@property (copy, nonatomic) NSArray *tags;
@property (nonatomic) TypeEnum type;
@property (copy, nonatomic) NSString *photoUrl;

@end
