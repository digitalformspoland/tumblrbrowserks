//
//  TumblrTableViewController.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "TumblrTableViewController.h"

@interface TumblrTableViewController ()

@end

@implementation TumblrTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewModel.delegate = self;
    self.tableView.delegate = self.viewModel;
    self.tableView.dataSource = self.viewModel;
    self.tumblrSearchBar.delegate = self.viewModel;
    self.navigationController.delegate = self.viewModel;
}

- (void)reloadTableViewData{
    [self.tableView reloadData];
}

-(void)setNavigationTitle:(NSString *)title{
    [self setTitle:title];
}

-(void)showFailureAlert{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Failure" message:@"Request Failed" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)didSelectPost:(PostKO*)post{
    DetailViewController *detailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"detailViewController"];
    detailVC.currentPost = post;
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end
