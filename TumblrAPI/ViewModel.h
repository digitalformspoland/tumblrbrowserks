//
//  ViewModel.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageView+AFNetworking.h"
#import "ViewModelProtocol.h"

#import "InteractorProtocol.h"


#import "StandardPostTableViewCell.h"
#import "PhotoTableViewCell.h"
#import "PostKO.h"
#import "TumbleLogKO.h"


@interface ViewModel : NSObject <ViewModelProtocol>

@property (weak, nonatomic) id<ViewModelDelegate>delegate;
@property (strong, nonatomic) id<InteractorProtocol>interactor;

@property (strong, readonly) NSArray *tableDataArray;
@property (strong, readonly) TumbleLogKO *tumbleLoKOg;

-(instancetype)initWithInteractor:(id<InteractorProtocol>)interactor;

@end
