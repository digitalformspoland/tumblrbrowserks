//
//  PostTableViewCell.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 12.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StandardPostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mainCellLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *tagLbl;

@end
