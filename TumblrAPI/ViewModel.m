//
//  ViewModel.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "ViewModel.h"

@implementation ViewModel

-(instancetype)initWithInteractor:(id<InteractorProtocol>)interactor{
    self = [super init];
    if(self){
        _interactor = interactor;
    }
    return self;
}

-(void)getDataForQuery:(NSString*)queryString{
    [self.interactor getDataFormServiceForQueryString:queryString withCompletionBlock:^(id responseObject) {
        // tumbleLog
        _tumbleLoKOg = [(NSArray*)responseObject lastObject];
        [self.delegate setNavigationTitle:self.tumbleLoKOg.title];
        [responseObject removeLastObject];
        
        //posts
        _tableDataArray = responseObject;
        [self.delegate reloadTableViewData];
    } failureBlock:^(NSError *error) {
        [self.delegate showFailureAlert];
    }];
}

#pragma mark - UISearchBar delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    [self getDataForQuery:searchBar.text];
}

#pragma mark - UITableView delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tableDataMutableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PostKO *post = self.tableDataMutableArray[indexPath.row];
    UITableViewCell *cell;
    switch (post.type) {
        case RegularTypeKO:{
            StandardPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"postCell" forIndexPath:indexPath];
            cell.mainCellLbl.text = post.regularTitle;
            cell.dateLbl.text = [NSString stringWithFormat:@"%@", post.date];
            cell.tagLbl.text = [post.tags componentsJoinedByString:@", "];
            return cell;
        }
            break;
        case PhotoTypeKO:{
            PhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
            cell.titleLbl.text = post.regularTitle;
            cell.dateLbl.text = [NSString stringWithFormat:@"%@", post.date];
            cell.tagLbl.text = [post.tags componentsJoinedByString:@", "];
            
            NSURL *photoUrl = [NSURL URLWithString:post.photoUrl];
            [cell.photoView setImageWithURL:photoUrl placeholderImage:nil];
            
            return cell;
        }
            break;
        default:
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PostKO *post = self.tableDataMutableArray[indexPath.row];
    switch (post.type) {
        case RegularTypeKO:
            return 77.0f;
            break;

        case PhotoTypeKO:
            return 332.0f;
            break;
            
        default:
            break;
    }
    return 77.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PostKO *post = self.tableDataMutableArray[indexPath.row];
    [self.delegate didSelectPost:post];
}

#pragma mark - Lazy Loaders

-(NSArray *)tableDataMutableArray{
    if (!_tableDataArray) {
        _tableDataArray = [[NSArray alloc]init];
    }
    return _tableDataArray;
}



@end
