//
//  DetailViewController.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 12.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostKO.h"


@interface DetailViewController : UIViewController <UIWebViewDelegate>


@property (strong, nonatomic) id currentPost;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
