//
//  DataParser.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataParserProtocol.h"
#import "PostKO.h"
#import "TumbleLogKO.h"
#import "NSDictionary+NSNull.h"

@interface DataParser : NSObject <DataParserProtocol>

@property (strong, nonatomic) NSDateFormatter *dateFormatter;


@end
