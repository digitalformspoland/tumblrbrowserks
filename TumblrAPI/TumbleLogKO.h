//
//  TumbleLogKO.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 12.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TumbleLogKO : NSObject
@property (copy, nonatomic) NSString *cname;
@property (copy, nonatomic) NSString *desc;
@property (copy, nonatomic) NSArray *feeds;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *title;

@end
