//
//  Interactor.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 04.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "InteractorProtocol.h"
#import "DataParserProtocol.h"
#import "ServiceProtocol.h"


@interface Interactor : NSObject <InteractorProtocol>

@property (strong, nonatomic) id<ServiceProtocol>service;
@property (strong, nonatomic) id<DataParserProtocol>parser;

- (instancetype)initWithParser:(id<DataParserProtocol>)parser andWithService:(id<ServiceProtocol>)service;

@end
