//
//  ViewModelDeleate.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 04.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostKO.h"

@protocol ViewModelDelegate <NSObject>

-(void)reloadTableViewData;
-(void)setNavigationTitle:(NSString*)title;
-(void)didSelectPost:(PostKO*)post;
-(void)showFailureAlert;

@end