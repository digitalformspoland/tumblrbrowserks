//
//  DetailViewController.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 12.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "DetailViewController.h"
#import "PostKO.h"

@interface DetailViewController ()

@end

@implementation DetailViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    self.webView.delegate = self;
    [self populateWebView:self.webView withData:self.currentPost];
}

-(void)populateWebView:(UIWebView*)webView withData:(id)data{
    PostKO *post = data;
    
    switch (post.type) {
        case PhotoTypeKO:{
            NSURL *url = [NSURL URLWithString:post.photoUrl];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [webView loadRequest:request];
        }
            break;
        case RegularTypeKO:
            [webView loadHTMLString:post.regularBody baseURL:nil];

            break;
        default:
            break;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                   message:@"Request Failed"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
