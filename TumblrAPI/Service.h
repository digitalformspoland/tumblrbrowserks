//
//  Service.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceProtocol.h"
#import "API.h"

@interface Service : NSObject <ServiceProtocol>



@end
