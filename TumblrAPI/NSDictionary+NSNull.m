//
//  NSDictionary+NSNull.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 13.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "NSDictionary+NSNull.h"

@implementation NSDictionary (NSNull)


- (id)valueOrNilForKey:(NSString *)key
{
    id value = self[key];
    if (((id)value == [NSNull null])) {
        value = nil;
    }
    return value;
}

@end
