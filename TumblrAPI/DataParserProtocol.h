//
//  DataParserProtocol.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PostKO.h"
#import "TumbleLogKO.h"

@protocol DataParserProtocol <NSObject>

-(PostKO*)parseResponseJSONData:(id)responseObject;
-(TumbleLogKO*)parseTumblelogJSONData:(id)responseObject;

@end