//
//  DataParser.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "DataParser.h"

@implementation DataParser

-(PostKO*)parseResponseJSONData:(id)responseObject
{
    PostKO *postKO = [[PostKO alloc]init];
    postKO.bookmarklet = [responseObject valueOrNilForKey:@"bookmarklet"];
    postKO.date = [responseObject valueOrNilForKey:@"date"];
    postKO.identifier = [responseObject valueOrNilForKey:@"id"];
    postKO.reblogKey = [responseObject valueOrNilForKey:@"reblogKeys"];
    postKO.regularBody = [responseObject valueOrNilForKey:@"regular-body"];
    postKO.regularTitle = [responseObject valueOrNilForKey:@"regular-title"];
    postKO.slug = [responseObject valueOrNilForKey:@"slug"];
    postKO.tags = [responseObject valueOrNilForKey:@"tags"];
    
    
    NSString *type = [responseObject valueOrNilForKey:@"type"];
    if (type && [type isEqualToString:@"regular"]) {
        postKO.type = RegularTypeKO;
    }else if(type && [type isEqualToString:@"photo"]){
        postKO.type = PhotoTypeKO;
        postKO.photoUrl = [responseObject valueOrNilForKey:@"photo-url-400"];
    }else{
        return nil;
    }
    

    return postKO;
}

-(TumbleLogKO*)parseTumblelogJSONData:(id)responseObject{
    TumbleLogKO *tumbleLog = [[TumbleLogKO alloc]init];
    tumbleLog.cname = [responseObject valueOrNilForKey:@"cname"];
    tumbleLog.desc = [responseObject valueOrNilForKey:@"description"];
    tumbleLog.feeds = [responseObject valueOrNilForKey:@"feeds"];
    tumbleLog.name = [responseObject valueOrNilForKey:@"name"];
    tumbleLog.title = [responseObject valueOrNilForKey:@"title"];
    return tumbleLog;
}

@end
