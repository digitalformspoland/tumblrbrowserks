//
//  TumblrTableViewController.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewModelProtocol.h"
#import "ViewModelDelegate.h"
#import "DetailViewController.h"
#import "PostKO.h"


@interface TumblrTableViewController : UITableViewController <ViewModelDelegate>

@property (strong, nonatomic) id<ViewModelProtocol>viewModel;
@property (weak, nonatomic) IBOutlet UISearchBar *tumblrSearchBar;

@end
