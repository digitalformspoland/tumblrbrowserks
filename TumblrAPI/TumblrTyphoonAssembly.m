//
//  TyphoonAssembly.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "TumblrTyphoonAssembly.h"

@implementation TumblrTyphoonAssembly

-(TumblrTableViewController*)tumblrTableViewController{
    return [TyphoonDefinition withClass:[TumblrTableViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(viewModel) with:[self viewModel]];
    }];
}

-(id<ViewModelProtocol>)viewModel{
    return  [TyphoonDefinition withClass:[ViewModel class] configuration:^(TyphoonDefinition *definition) {
        [definition injectMethod:@selector(initWithInteractor:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[self interactor]];
        }];
    }];
}

-(id<InteractorProtocol>)interactor{
    return [TyphoonDefinition withClass:[Interactor class] configuration:^(TyphoonDefinition *definition) {
        [definition injectMethod:@selector(initWithParser:andWithService:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[self dataParser]];
            [method injectParameterWith:[self service]];
        }];
    }];
}

-(id<ServiceProtocol>)service{
    return [TyphoonDefinition withClass:[Service class]];
}

-(id<DataParserProtocol>)dataParser{
    return [TyphoonDefinition withClass:[DataParser class]];
}

@end
