//
//  Service.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "Service.h"
#import "AFNetworking.h"

@implementation Service


-(void)downloadTumblrDataForSearchQueryString:(NSString*)queryString withCompletion:(completion)completion failure:(failure)failure{
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"text/javascript", @"application/json", @"application/javascript", nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [manager GET:[NSString stringWithFormat:@"http://%@.tumblr.com/api/read/json", [self searchQueryFormat:queryString]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *json = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        json = [json stringByReplacingOccurrencesOfString:@"var tumblr_api_read = " withString:@""];
        json = [json stringByReplacingOccurrencesOfString:@";" withString:@""];
        json = [json stringByReplacingOccurrencesOfString:@"\u00a0" withString:@"&nbsp;"];
        
        __strong NSError *jsonError;
        NSDictionary *notesJSON = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&jsonError];
        
        if (!notesJSON) {
            failure(jsonError);
            return;
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        completion(notesJSON);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failure(error);
    }];
}

-(NSString*)searchQueryFormat:(NSString*)queryString{
    NSCharacterSet *expectedCharSet = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *searchTerm = [queryString stringByAddingPercentEncodingWithAllowedCharacters:expectedCharSet];
    return searchTerm;
}

@end
