//
//  TyphoonAssembly.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import "TumblrTableViewController.h"

#import "DataParserProtocol.h"
#import "DataParser.h"

#import "ServiceProtocol.h"
#import "Service.h"

#import "ViewModelProtocol.h"
#import "ViewModel.h"

#import "InteractorProtocol.h"
#import "Interactor.h"

@interface TumblrTyphoonAssembly : TyphoonAssembly

-(TumblrTableViewController*)tumblrTableViewController;
-(id<ViewModelProtocol>)viewModel;
-(id<InteractorProtocol>)interactor;
-(id<ServiceProtocol>)service;
-(id<DataParserProtocol>)dataParser;

@end
