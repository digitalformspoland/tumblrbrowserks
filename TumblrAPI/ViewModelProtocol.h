//
//  ViewModelProtocol.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ViewModelDelegate.h"

@protocol ViewModelProtocol <NSObject , UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) id<ViewModelDelegate>delegate;

@end