//
//  Interactor.m
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 04.04.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//

#import "Interactor.h"

@implementation Interactor

- (instancetype)initWithParser:(id<DataParserProtocol>)parser andWithService:(id<ServiceProtocol>)service {
    self = [super init];
    if (self) {
        _parser = parser;
        _service = service;
    }
    return self;
}

-(void)getDataFormServiceForQueryString:(NSString*)string withCompletionBlock:(completion)completionBlock failureBlock:(failure)failureBlock{
    
    [self.service downloadTumblrDataForSearchQueryString:string withCompletion:^(id responseObject) {
        NSMutableArray *postKOArray = [NSMutableArray new];
        NSArray *responsePostArray = [responseObject objectForKey:@"posts"];
        
        
        int iCount = (int)responsePostArray.count;
    
        for (int i = 0; i<iCount; i++) {
            PostKO *postKO = [self.parser parseResponseJSONData:responsePostArray[i]];
            if (postKO) {
                [postKOArray addObject:postKO];
            }
        }
        
        NSDictionary *tumbleLogData = [responseObject objectForKey:@"tumblelog"];
        [postKOArray addObject:[self.parser parseTumblelogJSONData:tumbleLogData]];
        
        completionBlock(postKOArray);
        
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

@end
