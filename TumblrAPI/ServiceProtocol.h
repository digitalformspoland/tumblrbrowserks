//
//  ServiceProtocol.h
//  TumblrAPI
//
//  Created by Kamil Sokolowski on 31.03.2016.
//  Copyright © 2016 DigitalForms. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ServiceProtocol <NSObject>

typedef void(^completion)(id responseObject);
typedef void(^failure)(NSError* error);

-(void)downloadTumblrDataForSearchQueryString:(NSString*)queryString
                               withCompletion:(completion)completion
                                      failure:(failure)failure;

@end